package binnie.extratrees.api;

import net.minecraft.item.ItemStack;

public abstract interface IDesignMaterial {
	public abstract ItemStack getStack();
	public abstract String getName();
	public abstract int getColour();
}
