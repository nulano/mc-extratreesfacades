package binnie.extratrees.api;

import java.util.Collection;
import java.util.List;
import net.minecraft.item.ItemStack;

public abstract interface ICarpentryInterface
{
	public abstract boolean registerCarpentryWood(int paramInt, IDesignMaterial paramIDesignMaterial);

	public abstract int getCarpentryWoodIndex(IDesignMaterial paramIDesignMaterial);

	public abstract IDesignMaterial getWoodMaterial(int paramInt);

	public abstract boolean registerDesign(int paramInt, IDesign paramIDesign);

	public abstract int getDesignIndex(IDesign paramIDesign);

	public abstract IDesign getDesign(int paramInt);

//	public abstract ILayout getLayout(IPattern paramIPattern, boolean paramBoolean);

	public abstract IDesignMaterial getWoodMaterial(ItemStack paramItemStack);

//	public abstract boolean registerDesignCategory(IDesignCategory paramIDesignCategory);

//	public abstract IDesignCategory getDesignCategory(String paramString);

//	public abstract Collection<IDesignCategory> getAllDesignCategories();

	public abstract List<IDesign> getSortedDesigns();
}
