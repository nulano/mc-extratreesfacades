package binnie.extratrees.carpentry;

import binnie.extratrees.api.IDesignMaterial;
import net.minecraft.block.material.Material;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public enum DesignSystem {
	Wood, Glass;

	public Material getMaterial() {
		return null;
	}

	public IDesignMaterial getMaterial(int id) {
		return null;
	}

	public IDesignMaterial getDefaultMaterial2() {
		return null;
	}

	public int getMaterialIndex(IDesignMaterial mat) {
		return 0;
	}
}
