package binnie.extratrees.carpentry;

import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignMaterial;
import net.minecraft.item.ItemStack;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public class ModuleCarpentry {
	public static DesignBlock getDesignBlock(DesignSystem system, int meta) {
		return null;
	}
	@Deprecated
	public static int getMetadata(int plank1, int plank2, int design, int rotation, int facing) {
		return 0;
	}
	@Deprecated
	public static int getBlockMetadata(DesignSystem system, DesignBlock block) {
		return 0;
	}
	@Deprecated
	public static int getItemMetadata(DesignSystem system, DesignBlock block) {
		return 0;
	}
	public static ItemStack getItemStack(BlockCarpentry block, IDesignMaterial type1, IDesignMaterial type2, IDesign design) {
		return null;
	}
}
