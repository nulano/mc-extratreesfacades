package binnie.extratrees.carpentry;

import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignMaterial;
import net.minecraft.util.IIcon;
import net.minecraftforge.common.util.ForgeDirection;

@SuppressWarnings("UnusedParameters")
public class DesignBlock {
	DesignBlock(DesignSystem system, IDesignMaterial primaryWood, IDesignMaterial secondaryWood, IDesign design, int rotation, ForgeDirection dir) {}
	public IDesign getDesign() {
		return null;
	}
	public IDesignMaterial getPrimaryMaterial() {
		return null;
	}
	public IDesignMaterial getSecondaryMaterial() {
		return null;
	}
	public int getPrimaryColour()
	{
		return getPrimaryMaterial().getColour();
	}
	public int getSecondaryColour()
	{
		return getSecondaryMaterial().getColour();
	}
	public IIcon getPrimaryIcon(DesignSystem system, ForgeDirection dir) {
		return null;
	}
	public IIcon getSecondaryIcon(DesignSystem system, ForgeDirection dir) {
		return null;
	}
	public int getRotation() {
		return 0;
	}
	public ForgeDirection getFacing() {
		return null;
	}
}
