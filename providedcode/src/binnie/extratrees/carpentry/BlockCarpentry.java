package binnie.extratrees.carpentry;

import net.minecraft.block.BlockContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public class BlockCarpentry extends BlockContainer {

	public BlockCarpentry(DesignSystem ds) {
		super(null);
	}

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return null;
	}

	public DesignSystem getDesignSystem() {
		return null;
	}

	public static final ForgeDirection[] RENDER_DIRECTIONS = {};

	public static int getMetadata(int plank1, int plank2, int design) {
		return 0;
	}

	public ItemStack getItemStack(int plank1, int plank2, int design) {
		return null;
	}

}
