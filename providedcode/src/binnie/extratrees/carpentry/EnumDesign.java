package binnie.extratrees.carpentry;

import binnie.extratrees.api.IDesign;

public enum EnumDesign
		implements IDesign
{
	Blank("Blank"),  Octagon("Octagon"),  Diamond("Diamond"),  Ringed("Ringed"),  Squared("Squared"),  Multiply("Multiply"),  Halved("Halved"),  Striped("Striped"),  ThinStriped("Thin Striped"),  Chequered("Full Chequered"),  Tiled("Full Tiled"),  ChequeredB("Chequered"),  TiledB("Tiled"),  VeryThinCorner("Very Thin Cornered"),  ThinCorner("Thin Cornered"),  Corner("Cornered"),  ThickCorner("Thick Cornered"),  Edged("Edged"),  ThinEdged("Thin Edged"),  ThinBarred("Thin Barred"),  Barred("Barred"),  ThickBarred("Thick Barred"),  Diagonal("Diagonal"),  ThickDiagonal("Thick Diagonal"),  ThinSaltire("Thin Saltire"),  Saltire("Saltire"),  ThickSaltire("Thick Saltire"),  ThinCrossed("Thin Crossed"),  Crossed("Crossed"),  ThickCrossed("Thick Crossed"),  ThinTSection("Thin T Section"),  TSection("T Section"),  ThickTSection("Thick T Section"),  ThinBarredCorner("Thin Barred Corner"),  BarredCorner("Barred Corner"),  ThickBarredCorner("Thick Barred Corner"),  ThinStripedCorner("Thin Striped Corner"),  StripedCorner("Striped Corner"),  Emblem1("Emblem 1"),  Emblem2("Emblem 2"),  Emblem3("Emblem 3"),  Emblem4("Emblem 4"),  Emblem5("Emblem 5"),  LetterA("Letter A"),  LetterB("Letter B"),  LetterC("Letter C"),  LetterD("Letter D"),  LetterE("Letter E"),  LetterF("Letter F"),  LetterG("Letter G"),  LetterH("Letter H"),  LetterI("Letter I"),  LetterJ("Letter J"),  LetterK("Letter K"),  LetterL("Letter L"),  LetterM("Letter M"),  LetterN("Letter N"),  LetterO("Letter O"),  LetterP("Letter P"),  LetterQ("Letter Q"),  LetterR("Letter R"),  LetterS("Letter S"),  LetterT("Letter T"),  LetterU("Letter U"),  LetterV("Letter V"),  LetterW("Letter W"),  LetterX("Letter X"),  LetterY("Letter Y"),  LetterZ("Letter Z"),  ThinCurvedCrossed("Thin Curved Crossed"),  ThinCurvedBarredCorner("Thin Curved Barred Corner"),  CurvedBarredCorner("Curved Barred Corner"),  ThinCurvedCorner("Thin Curved Corner"),  CurvedCorner("Curved Corner"),  ThinCurvedTSection("Thin Curved T Section"),  CurvedTSection("Curved T Section"),  BarredEnd("Barred End"),  DiagonalCorner("Diagonal Corner"),  DiagonalTSection("Diagonal T Section"),  DiagonalCurvedCorner("Diagonal Curved Corner"),  DiagonalCurvedTSection("Diagonal Curved T Section"),  OrnateBarred("Ornate Barred"),  SplitBarred("Split Barred"),  SplitBarredCorner("Split Barred Corner"),  SplitBarredTSection("Split Barred T Section"),  SplitCrossed("Split Crossed"),  SplitBarredEnd("Split Barred End"),  OrnateThinBarred("Ornate Thin Barred"),  Circle("Circle"),  Plus("Plus"),  Creeper("Creeper"),  OrnateStripedCorner("Ornate Striped Corner"),  Test("Testing Block"),  DiagonalHalved("Diagonal Halved"),  Diagonal1Edged("Cornered Diagonal"),  Diagonal2Edged("Opposite Cornered Diagonal"),  ThickDiagonal1Edged("Thick Cornered Diagonal"),  ThinBarredEnd("Thin Barred End"),  ThickBarredEnd("Thick Barred End"),  OverlappedBarred("Overlapped Barred"),  OverlappedSplitBarred("Overlapped Split Barred");

	String name;

	private EnumDesign(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
