package nulano.mc.extratreesfacades;

import binnie.extratrees.carpentry.DesignSystem;
import nulano.mc.extratreesfacades.block.BlockSingleMaterial;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public final class R {

	@SuppressWarnings("SpellCheckingInspection")
	public static final String modid = "nulano.extratreesfacades";
	public static final String modname = "Facades for ExtraTrees";
	public static final String version = "1.0";
	public static final String dependencies = "required-after:BuildCraft|Silicon@6.1.5n;required-after:ExtraTrees@2.0-dev5";

	public static final class name {

		public static final String key_category = "key.category";
		public static final String key_debugHeld = "key.debug_held";

		public static String blockl(BlockSingleMaterial b) {
			return String.format("%s %s", b.design.getName(), b.system == DesignSystem.Glass? "Stained Glass":"Wooden Tile");
		}
		public static String block(BlockSingleMaterial b) {
			return String.format("%s.%s.%s", "renderblock", b.design.getName(), b.system.name()+(b.offset>>>4));
		}

	}

}
