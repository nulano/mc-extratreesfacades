package nulano.mc.extratreesfacades.block;

import binnie.extratrees.api.IDesign;
import binnie.extratrees.carpentry.*;
import buildcraft.core.render.RenderUtils;
import buildcraft.transport.render.PipeRendererWorld;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;
import nulano.mc.extratreesfacades.ETFacades;

import java.util.List;

/**
 * Created by Nulano on 16. 11. 2014.
 *
 * @author Nulano
 */
public class BlockSingleMaterial extends Block {

	public final DesignSystem system;
	public final IDesign design;
	public final int offset;

	public BlockSingleMaterial(DesignSystem ds, IDesign design) {
		this(ds, design, 0);
	}

	public BlockSingleMaterial(DesignSystem ds, IDesign design, int section) {
		super(ds.getMaterial());
		this.system = ds;
		this.design = design;
		offset = section << 4;
//		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public int colorMultiplier(IBlockAccess ba, int x, int y, int z) {
		return colorMultiplier(ba.getBlockMetadata(x, y, z));
	}

	public int colorMultiplier(int metadata) {
//		DesignBlock db = getDesignBlock(metadata);
		return system.getMaterial((PipeRendererWorld.renderPass == 1 || BlockRenderer.isSecondLayer()? metadata : metadata) + offset).getColour();
	}

	@Override
	public IIcon getIcon(int side, int metadata) {
//		RenderUtils.setGLColorFromInt(colorMultiplier(metadata));
		DesignBlock db = getDesignBlock(metadata);
		return BlockRenderer.isSecondLayer()?
				db.getSecondaryIcon(system, BlockCarpentry.RENDER_DIRECTIONS[side]) :
				db.getPrimaryIcon(system, BlockCarpentry.RENDER_DIRECTIONS[side]);
	}

	public DesignBlock getDesignBlock(int metadata) {
		return PackagePrivateHelper.createDesignBlock(system, system.getMaterial(metadata+offset), system.getMaterial(metadata+offset), design, 0, ForgeDirection.UP);
	}

	@Override
	public int getRenderType() {
		return ETFacades.renderID;
	}

	@Override
	public int damageDropped(int metadata) {
		return metadata;
	}

	@Override
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("unchecked")
	public void getSubBlocks(Item item, CreativeTabs tab, List subItems) {
		for (int ix = 0; ix < 16; ix++) {
			if (system.getMaterial(ix+offset) != null)
				subItems.add(new ItemStack(item, 1, ix));
		}
	}

	@Override
	public boolean canRenderInPass(int pass) {
		return pass == 0 || pass == 1;
	}
}
