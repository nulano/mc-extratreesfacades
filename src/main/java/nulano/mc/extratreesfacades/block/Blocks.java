package nulano.mc.extratreesfacades.block;

import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.ExtraTrees;
import binnie.extratrees.api.CarpentryManager;
import binnie.extratrees.carpentry.*;
import binnie.extratrees.machines.DesignerType;
import buildcraft.BuildCraftTransport;
import buildcraft.api.recipes.BuildcraftRecipeRegistry;
import buildcraft.transport.ItemFacade;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.ForgeInternalHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;
import nulano.mc.extratreesfacades.R;
import nulano.mc.extratreesfacades.client.DebugHeldItemEvHandler;

import java.util.*;

/**
 * Created by Nulano on 16. 11. 2014.
 *
 * @author Nulano
 */
//@GameRegistry.ObjectHolder(R.modid)
public class Blocks {

	public static final class FacadeData {
//		public final boolean blocks;
		public final Block block;
		public final int meta;
		public final ItemFacade.FacadeState facadeState;

		public FacadeData(Block block, int meta) {
			this(block, meta, false);
		}

		@Deprecated
		public FacadeData(Block block, int meta, boolean doesBlock) {
			this.block = block;
			this.meta = meta;
//			this.blocks = doesBlock;

			facadeState = new ItemFacade.FacadeState(block, meta, null);
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof FacadeData &&
					block == ((FacadeData) obj).block &&
					meta == ((FacadeData) obj).meta /*&&
					blocks == ((FacadeData) obj).blocks*/;
		}

		@Override
		public int hashCode() {
			return (meta/* << (blocks? 1:0)*/) * 31 + block.hashCode();
		}

		@Override
		public String toString() {
			return "FacadeData(" + block + ", " + meta + /*// ", " + blocks + //*/ ')';
		}
	}

	public static BlockSingleMaterial glassPlain;
	public final static Map<FacadeData, ItemStack> facadeItemStacks = new HashMap<FacadeData, ItemStack>();
	public final static Map<Integer, BlockSingleMaterial> list_woodPlain = new HashMap<Integer, BlockSingleMaterial>();
	@SuppressWarnings("FieldCanBeLocal")
	private static Iterable<Integer> woodIndeces;

	private static void registerSingle(BlockSingleMaterial b) {
		GameRegistry.registerBlock(b, ItemBlockSingleMaterial.class, R.name.block(b));
//		addFacades(b);
	}

	public static void addFacades(BlockSingleMaterial b) {
		List<ItemStack> l = new LinkedList<ItemStack>();
		b.getSubBlocks(Item.getItemFromBlock(b), null, l);
		if (b.system != DesignSystem.Glass) {
			for (ItemStack is : l) {
				addFacade(b, is.getItemDamage(), DesignerType.Woodworker, b.getDesignBlock(is.getItemDamage()));
				addFacade(b, is.getItemDamage(), DesignerType.Panelworker, b.getDesignBlock(is.getItemDamage()));
			}
		} else {
			for (ItemStack is : l)
				addFacade(b, is.getItemDamage(), DesignerType.GlassWorker, b.getDesignBlock(is.getItemDamage()));
		}
	}

	public static ItemStack getFacadeItemStack(Block b, int meta) {
		FacadeData fd = new FacadeData(b, meta);
		synchronized (facadeItemStacks) {
			ItemStack is = facadeItemStacks.get(fd);
			if (is != null)
				return is;
			is = ItemFacade.getFacade(fd.facadeState);
			ItemFacade.allFacades.add(is);
			facadeItemStacks.put(fd, is);
			return is;
		}
	}

	/** @see buildcraft.transport.ItemFacade#addFacade(String, net.minecraft.item.ItemStack) */
	public static void addFacade(Block block, int meta, DesignerType dt, DesignBlock db/*m*/) {
		boolean panel = dt == DesignerType.Panelworker;

		ItemStack facade = getFacadeItemStack(block, meta);

		String id = "buildcraft:facade{" + dt.name() + '@' + Block.blockRegistry.getNameForObject(block) + ':' + meta + '}';

		ItemStack facades = facade.copy();
		facades.stackSize = panel? 2:6;

		ItemStack is = dt.getBlock(db.getPrimaryMaterial(), db.getSecondaryMaterial(), db.getDesign());
		System.out.println(id);
		DebugHeldItemEvHandler.dumpIS(is);

		BuildcraftRecipeRegistry.assemblyTable
				.addRecipe(id, panel? 3000:8000, facades, new ItemStack(BuildCraftTransport.pipeStructureCobblestone, panel? 1:3), is);
	}

	public static void init() {
		Item.getItemFromBlock(ExtraTrees.blockCarpentry).setHasSubtypes(true);

		registerSingle(glassPlain = new BlockSingleMaterial(DesignSystem.Glass, EnumDesign.Blank));

		woodIndeces = PackagePrivateHelper.getWoodIndeces();
		for (Integer i : woodIndeces) {
			System.out.println("Detected wood type: "+i+'='+ DesignSystem.Wood.getMaterial(i));
			if (list_woodPlain.containsKey(i/16)) continue;
			System.out.println("Adding wood block group: " + i / 16);
			list_woodPlain.put(i / 16, new BlockSingleMaterial(DesignSystem.Wood, EnumDesign.Blank, i / 16));
		}
		for (BlockSingleMaterial block : list_woodPlain.values()) {
			registerSingle(block);
		}
	}

	public static void addFacades() {
		addFacades(glassPlain);

		for (BlockSingleMaterial block : list_woodPlain.values()) {
			addFacades(block);
		}
	}

//	/**
//	 * http://stackoverflow.com/a/6828887/1648883
//	 * @param begin inclusive
//	 * @param end exclusive
//	 * @return list of integers from begin to end
//	 */
//	public static List<Integer> range(final int begin, final int end) {
//		return new AbstractList<Integer>() {
//			@Override
//			public Integer get(int index) {
//				return begin + index;
//			}
//
//			@Override
//			public int size() {
//				return end - begin;
//			}
//		};
//	}

}
