package nulano.mc.extratreesfacades.block;

import binnie.extratrees.api.IDesignMaterial;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import nulano.mc.extratreesfacades.R;

/**
 * Created by Nulano on 16. 11. 2014.
 *
 * @author Nulano
 */
public class ItemBlockSingleMaterial extends ItemBlock {

	public ItemBlockSingleMaterial(Block block) {
		super(block);
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	@Override
	public int getMetadata(int metadata) {
		return metadata;
	}

	@Override
	public String getUnlocalizedName(ItemStack is) {
		if (is.getItem() != this)
			throw new RuntimeException();
		BlockSingleMaterial block = (BlockSingleMaterial) field_150939_a;
		IDesignMaterial material = block.system.getMaterial(is.getItemDamage()+block.offset);
		return R.name.block(block)+(material == null? "":'.'+ material.getName());
	}

	@Override
	public String getItemStackDisplayName(ItemStack is) {
		String s = super.getItemStackDisplayName(is);
		if (!s.endsWith(".name")) return s;
		if (is.getItem() != this)
			throw new RuntimeException();
		BlockSingleMaterial block = (BlockSingleMaterial) field_150939_a;
		IDesignMaterial material = block.system.getMaterial(is.getItemDamage()+block.offset);
		return (material == null? "":material.getName()+' ')+ R.name.blockl(block);
	}

	// FIXME BC stores this in BlockGenericPipe.facadeRenderColor but not in the rendered block (BC FakeBlock). Bug?
	@Override
	public int getColorFromItemStack(ItemStack p_82790_1_, int p_82790_2_) {
		return ((BlockSingleMaterial) field_150939_a).colorMultiplier(p_82790_1_.getItemDamage());
	}
}
