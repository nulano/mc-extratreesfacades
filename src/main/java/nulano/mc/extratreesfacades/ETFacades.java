package nulano.mc.extratreesfacades;

import binnie.extratrees.ExtraTrees;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.item.ItemStack;
import nulano.mc.extratreesfacades.block.BlockRenderer;
import nulano.mc.extratreesfacades.block.Blocks;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
@Mod(modid = R.modid, name = R.modname, version = R.version, dependencies = R.dependencies)
public class ETFacades {

	@SidedProxy(clientSide = "nulano.mc.extratreesfacades.ProxyClient", serverSide = "nulano.mc.extratreesfacades.ProxyServer")
	public static ProxyCommon proxy;

	public static int renderID;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent ev) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
		Blocks.init();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent ev) {
		renderID = RenderingRegistry.getNextAvailableRenderId();
		RenderingRegistry.registerBlockHandler(new BlockRenderer());

		proxy.registerKeybindings();
//		FMLInterModComms.sendMessage("BuildCraft|Silicon", "add-facade", new ItemStack(ExtraTrees.blockCarpentry, 1, 268567041));
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent ev) {
		//proxy.hideNeiItems();

		Blocks.addFacades();
	}

}
