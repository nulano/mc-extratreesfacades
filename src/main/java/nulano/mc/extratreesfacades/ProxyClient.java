package nulano.mc.extratreesfacades;

import binnie.extratrees.carpentry.DesignBlockRenderer;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import nulano.mc.extratreesfacades.client.Keybindings;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public class ProxyClient extends ProxyCommon {

	@Override
	public void registerKeybindings() {
		Keybindings.register();
	}

//	@Override
//	public void registerBlockRenderer(Block b) {
//		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(b), CarpentryItemRenderer.instance);
//	}
}
