package nulano.mc.extratreesfacades.client;

import binnie.core.block.TileEntityMetadata;
import binnie.extratrees.carpentry.BlockCarpentry;
import binnie.extratrees.carpentry.DesignBlock;
import binnie.extratrees.carpentry.ModuleCarpentry;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.block.Block;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.item.ItemStack;


/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public class DebugHeldItemEvHandler {

	public void run() {
		EntityClientPlayerMP player = FMLClientHandler.instance().getClientPlayerEntity();
		ItemStack is = player.getHeldItem();
		if (is == null) return;
		dumpIS(is);
	}

	public static void dumpIS(ItemStack is) {
		Block b = Block.getBlockFromItem(is.getItem());
//		System.out.println("render type: "+b.getRenderType());
		if (!(b instanceof BlockCarpentry)) {
			System.out.println("Invalid block: "+b);
		} else {
			BlockCarpentry bc = (BlockCarpentry) b;
			DesignBlock db = ModuleCarpentry.getDesignBlock(bc.getDesignSystem(), TileEntityMetadata.getItemDamage(is));
			if (db.getPrimaryMaterial() == db.getSecondaryMaterial())
				System.out.println(String.format("DesignBlock:%s:%s(%s,%s)[%s]{%s;%s}", is.getItemDamage(), TileEntityMetadata.getItemDamage(is), bc.getDesignSystem(), db.getDesign(), db.getPrimaryMaterial(), db.getRotation(), db.getFacing()));
			else
				System.out.println(String.format("DesignBlock:%s:%s(%s,%s)[%s;%s]{%s;%s}", is.getItemDamage(), TileEntityMetadata.getItemDamage(is), bc.getDesignSystem(), db.getDesign(), db.getPrimaryMaterial(), db.getSecondaryMaterial(), db.getRotation(), db.getFacing()));
		}
	}

	@SubscribeEvent
	public void handleKeyEvent(InputEvent.KeyInputEvent ev) {
		if (!Keybindings.debugHeld.isPressed())
			return;
		run();
	}

}
