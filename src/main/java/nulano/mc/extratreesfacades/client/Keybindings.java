package nulano.mc.extratreesfacades.client;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.client.settings.KeyBinding;
import nulano.mc.extratreesfacades.R;
import org.lwjgl.input.Keyboard;

/**
 * Created by Nulano on 15. 11. 2014.
 *
 * @author Nulano
 */
public class Keybindings {

	public static KeyBinding debugHeld;

	public static void register() {
		ClientRegistry.registerKeyBinding(debugHeld = new KeyBinding(R.name.key_debugHeld, Keyboard.KEY_F, R.name.key_category));
		FMLCommonHandler.instance().bus().register(new DebugHeldItemEvHandler());
	}
}
