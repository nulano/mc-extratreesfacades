package binnie.extratrees.carpentry;

import binnie.extratrees.api.IDesign;
import binnie.extratrees.api.IDesignMaterial;
import net.minecraftforge.common.util.ForgeDirection;

/**
 * Created by Nulano on 16. 11. 2014.
 *
 * @author Nulano
 */
public class PackagePrivateHelper {
	public static DesignBlock createDesignBlock(DesignSystem system, IDesignMaterial primaryWood, IDesignMaterial secondaryWood, IDesign design, int rotation, ForgeDirection dir) {
		return new DesignBlock(system, primaryWood, secondaryWood, design, rotation, dir);
	}
	public static Iterable<Integer> getWoodIndeces() {
		return CarpentryInterface.woodMap.keySet();
	}
}
